package com.nuance.org.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MyServlet2
 */
@WebServlet("/MyServlet2")
public class MyServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyServlet2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		try{
			response.setContentType("text/html");
			PrintWriter writer = response.getWriter();
			HttpSession session = request.getSession();
			String myName = (String)session.getAttribute("uname");
			String myPass = (String)session.getAttribute("upass");
			writer.print("Name:"+myName+"Pass:"+myPass);
			Map<String,String> map = new HashMap<>();
			map.put("Name", myName);
			map.put("Password",myPass);
			request.setAttribute("map", map);
			RequestDispatcher rd = request.getRequestDispatcher("result.jsp");
			rd.forward(request, response);

			writer.close();
			
		}catch(Exception exp) {
			System.out.println(exp);
		}
	}
	 protected void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
	    {
	        PrintWriter pw=res.getWriter();
	        res.setContentType("text/html");        
	        String tb=req.getParameter("table");
	        HttpSession session = req.getSession();
	        String table = (String) session.getAttribute("table");
	        pw.print("From DB servlet");
	        try
	        {
	             Class.forName("oracle.jdbc.driver.OracleDriver");
	             Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","system","sample");
	             Statement st=con.createStatement();
	             System.out.println("connection established successfully...!!");     

	             ResultSet rs=st.executeQuery("Select * from "+table);

	             pw.println("<table border=1>");
	                 while(rs.next())
	                 {
	                     pw.println("<tr><td>"+rs.getInt(1)+"</td><td>"+rs.getString(2)+"</td>"+
	                                      "<td>"+rs.getString(3)+"</td></tr>");
	                 }
	             pw.println("</table>");
	             pw.close();
	        }
	        catch (Exception e){
	            e.printStackTrace();
	        }

	    }
	
	

}
