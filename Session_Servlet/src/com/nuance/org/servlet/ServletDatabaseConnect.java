package com.nuance.org.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ServletDatabaseConnect
 */
@WebServlet("/ServletDatabaseConnect")
public class ServletDatabaseConnect extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletDatabaseConnect() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	 protected void doGet(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
	    {
	        PrintWriter pw=res.getWriter();
	        res.setContentType("text/html");        
	        String tb=req.getParameter("table");
	        HttpSession session = req.getSession();
	        String table = (String) session.getAttribute("table");
	        pw.print("From DB servlet");
	        pw.print(table);
	        try
	        {
	             Class.forName("oracle.jdbc.driver.OracleDriver");
	             Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","tester","password");
	             Statement st=con.createStatement();
	             System.out.println("connection established successfully...!!");     

	             ResultSet rs=st.executeQuery("Select * from "+table);

	             pw.println("<table border=1>");
	                 while(rs.next())
	                 {
	                     pw.println("<tr><td>"+rs.getInt(1)+"</td><td>"+rs.getString(2)+"</td>"+
	                                      "<td>"+rs.getString(3)+"</td></tr>");
	                 }
	             pw.println("</table>");
	             pw.close();
	        }
	        catch (Exception e){
	            e.printStackTrace();
	        }

	    }
	

}
